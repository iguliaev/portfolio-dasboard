(function($) {
  'use strict';
  $(function() {
    var assetListElement = $('.asset-list-anchor');
    $('.asset-add-fields-btn').on("click", function(event) {
      event.preventDefault();
        console.log("button clicked");
        var numInitialFileds = $('.asset-list-initial').children().length
        var numAddedFields = 5;
        for (var i = 0; i < numAddedFields; i++) {
            var itemId = i + numInitialFileds
            assetListElement.append(
                "<div class='form-group row'>" +
                    "<div class='col'>" +
                    "<input class='form-control p_input' id='assets-" + itemId +"-ticker' name='assets-" + itemId +"-ticker' placeholder='AAPL' type='text' value>" +
                    "</div>" +
                    "<div class='col'>" +
                    "<input class='form-control p_input' id='assets-" + itemId +"-amount' name='assets-" + itemId +"-amount' placeholder='0' type='text' value>" +
                    "</div>" +
                "</div>");
        }
    });

  });
})(jQuery);
