from app import db
from app.models import AssetInfo, User, Portfolio, Asset, AssetPrice

import click
import json

def register(app):
    @app.cli.command("testdata")
    def populate_test_data():

        user = User(username="test",email="test@localhost")

        portfolio = Portfolio(name='TestPortfolio',
                              description="This is a test portfolio",
                              owner=user)

        for info in AssetInfo.query.all():
            portfolio.assets.append(Asset(info=info, amount=100))

        db.session.add(user)
        db.session.add(portfolio)
        db.session.commit()
        print('populate-test-data')


    @app.cli.command("populate-db")
    def populate_db():

        user = User(username="test",email="test@localhost")

        portfolio = Portfolio(name='TestPortfolio',
                              description="This is a test portfolio",
                              owner=user)

        for info in AssetInfo.query.all():
            portfolio.assets.append(Asset(info=info, amount=100))

        db.session.add(user)
        db.session.add(portfolio)
        db.session.commit()

        print('populate-db')


    @app.cli.command("add-user")
    @click.argument('name')
    def add_user(name):
        user = User(username=name)
        db.session.add(user)
        db.session.commit()


    @app.cli.command("add-portfolio")
    @click.argument('name')
    def add_user(name):
        portfolio = Portfolio()
        user = User(username=name)
        db.session.add(user)
        db.session.commit()


    @app.cli.command("load-assets")
    @click.argument('filename', type=click.Path(exists=True))
    def load_assets(filename):
        with open(filename) as handle:
            data = json.load(handle)
            for asset in data['assets']:
                info = AssetInfo.query.filter_by(ticker=asset['ticker']).first()
                if info:
                    print("Update asset {}".format(asset['ticker']))
                    info.figi=asset['figi']
                    info.type=asset['type']
                    info.name=asset['name']
                    info.currency=asset['currency']
                    info.price=AssetPrice(price=asset['last_price'][1])
                else:
                    print("Add new asset {}".format(asset['ticker']))
                    db.session.add(
                        AssetInfo(ticker=asset['ticker'],
                                  figi=asset['figi'],
                                  type=asset['type'],
                                  name=asset['name'],
                                  currency=asset['currency'],
                                  price=AssetPrice(
                                      price=asset['last_price'][1])))

            db.session.commit()
