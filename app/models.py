from flask_login import UserMixin
from sqlalchemy import Binary, Column, Integer, String

from app import db, login_manager

from app.base.util import hash_pass


import decimal

class User(db.Model, UserMixin):

    __tablename__ = 'User'

    id = Column(Integer, primary_key=True)
    username = Column(String, unique=True)
    email = Column(String, unique=True)
    password = Column(Binary)
    portfolios = db.relationship('Portfolio', backref='owner', lazy='dynamic')

    def __init__(self, **kwargs):
        for property, value in kwargs.items():
            # depending on whether value is an iterable or not, we must
            # unpack it's value (when **kwargs is request.form, some values
            # will be a 1-element list)
            if hasattr(value, '__iter__') and not isinstance(value, str):
                # the ,= unpack of a singleton fails PEP8 (travis flake8 test)
                value = value[0]

            if property == 'password':
                value = hash_pass( value ) # we need bytes here (not plain str)

            setattr(self, property, value)

    def __repr__(self):
        return str(self.username)


@login_manager.user_loader
def user_loader(id):
    return User.query.filter_by(id=id).first()


@login_manager.request_loader
def request_loader(request):
    username = request.form.get('username')
    user = User.query.filter_by(username=username).first()
    return user if user else None


class Portfolio(db.Model):
    __tablename__ = 'Portfolio'

    id = Column(Integer, primary_key=True)
    name = Column(String, unique=True)
    description = Column(db.String(140))
    user_id = Column(db.Integer, db.ForeignKey('User.id'))

    assets = db.relationship('Asset',
                             backref=db.backref('portfolio'))

    def __repr__(self):
        return f"Portfolio(name={self.name}, description={self.description})"

    def add_asset(self, ticker, amount):
        info = AssetInfo.query.filter_by(ticker=ticker).first()
        if info:
            self.assets.append(Asset(info=info,amount=amount))

    def total_price(self):
        total = decimal.Decimal(0.0)

        for asset in self.assets:
            total += asset.total_price()
        return total


class AssetInfo(db.Model):
    __tablename__ = 'AssetInfo'

    id = Column(Integer, primary_key=True)
    ticker = Column(String, unique=True)
    figi = Column(String, unique=True)
    type = Column(String)
    name = Column(String)
    currency = db.Column(db.String)
    price = db.relationship('AssetPrice', uselist=False)

    def __repr__(self):
        return f"AssetInfo(id={self.id},ticker={self.ticker}," + \
            f"type={self.type},figi={self.figi},name='{self.name}'," + \
            f"currency={self.currency})"

    def currency_sign(self):
        signs = {'USD': '\u0024',
                 'RUB': '\u20bd',
                 'EUR': '\u20ac'}

        return signs.get(self.currency, self.currency)

class AssetPrice(db.Model):
    __tablename__ = 'AssetPrice'

    id = db.Column(db.Integer, primary_key=True)
    asset_id = db.Column(db.Integer, db.ForeignKey('AssetInfo.id'))
    price = db.Column(db.String)

    def __repr__(self):
        return f"AssetPrice(id={self.id},price={self.price})"


class Asset(db.Model):
    __tablename__ = 'Asset'

    id = db.Column(db.Integer, primary_key=True)
    portfolio_id = Column(Integer, db.ForeignKey('Portfolio.id'))
    asset_info_id = Column(Integer, db.ForeignKey('AssetInfo.id'))
    amount = Column(Integer)

    info = db.relationship(AssetInfo)

    def __repr__(self):
        return f"Asset(amount={self.amount})"

    def total_price(self):
        return self.amount * decimal.Decimal(self.info.price.price)

    def allocation(self):
        return self.total_price() / self.portfolio.total_price()
