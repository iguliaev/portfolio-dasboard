
from flask_wtf import FlaskForm
from wtforms import TextField, StringField, IntegerField, SubmitField, TextAreaField, FieldList, FormField
from wtforms.validators import DataRequired, Optional, Length

class AssetForm(FlaskForm):
    ticker = StringField('Ticker', validators=[Optional()])
    amount = IntegerField('Amount', validators=[Optional()])

class EditPortfolioForm(FlaskForm):
    name = TextField('Name',
                     id='portfolio_name',
                     validators=[DataRequired(message="No protfolio name provided")])
    description = TextAreaField('Description',
                                id='portfolio_description',
                                validators=[Length(min=0, max=140, message="Portfolio description is not provided")])
    assets = FieldList(FormField(AssetForm), min_entries=3)
    submit = SubmitField('Submit')
    cancel = SubmitField('Cancel')


class ViewEditDeleteForm(FlaskForm):
    edit = SubmitField('Edit')
    delete = SubmitField('Delete')

class DeleteConfirmationForm(FlaskForm):
    delete = SubmitField('Delete')
    cancel = SubmitField('Cancel')
