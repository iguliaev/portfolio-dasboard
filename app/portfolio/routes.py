from flask import flash, render_template, redirect, request, url_for
from flask_login import current_user, login_required

from app import db, login_manager
from app.portfolio import blueprint
from app.portfolio.forms import (
    EditPortfolioForm,
    ViewEditDeleteForm,
    DeleteConfirmationForm)
from app.models import User, Portfolio, Asset, AssetInfo

@blueprint.route('/new-portfolio', methods=['GET', 'POST'])
@login_required
def new_portfolio():
    form = EditPortfolioForm(request.form)

    # flash("validate on submit: {}".format(form.validate_on_submit()))
    # flash("validate: {}".format(form.validate()))
    # flash("errors: {}".format(form.errors))

    if form.validate_on_submit():
        portfolio = Portfolio(name=form.name.data,
                              description=form.description.data,
                              owner=current_user)

        for entry in form.assets.entries:
            portfolio.add_asset(entry.data['ticker'], entry.data['amount'])
            
        db.session.add(portfolio)
        db.session.commit()

        # flash("New Portfolio form. Name={}, description={}, assets={}".format(
        #     form.name.data, form.description.data, form.assets.entries))

        return redirect(url_for('home_blueprint.index'))

    return render_template('new-portfolio.html',
                           _title="New Portfolio",
                           form=form)

@blueprint.route('/view-portfolio', methods=['GET', 'POST'])
@login_required
def view_portfolio():

    form = ViewEditDeleteForm(request.form)

    id = request.args.get('id', 0, type=int)

    if form.validate_on_submit():
        print("Edit data value: {}".format(form.edit.data))
        print("Delete data value: {}".format(form.delete.data))

        if form.edit.data:
            return redirect(url_for('portfolio.edit_portfolio', id=id))
        if form.delete.data:
            return redirect(url_for('portfolio.delete_portfolio', id=id))

    portfolio = current_user.portfolios.filter_by(id=id).first_or_404()

    return render_template('view-portfolio.html',
                           _title="Portfolio View",
                           portfolio=portfolio,
                           form=form)


@blueprint.route('/delete-portfolio', methods=['GET', 'POST'])
@login_required
def delete_portfolio():
    form = DeleteConfirmationForm()

    portfolioId = request.args.get('id', 0, type=int)


    if form.validate_on_submit():
        portfolio = current_user.portfolios.filter_by(id=portfolioId).first()
        db.session.delete(portfolio)
        db.session.commit()
        return redirect(url_for('home_blueprint.index'))

    portfolio = current_user.portfolios.filter_by(id=portfolioId).first_or_404()
    return render_template('delete-portfolio.html',
                           _title="Delete Portfolio",
                           portfolio=portfolio,
                           form=form)

@blueprint.route('/edit-portfolio', methods=['GET', 'POST'])
@login_required
def edit_portfolio():
    form = EditPortfolioForm()

    id = request.args.get('id', 0, type=int)

    portfolio = current_user.portfolios.filter_by(id=id).first_or_404()
    
    if form.validate_on_submit():

        if form.submit.data:
            portfolio.name = form.name.data
            portfolio.description = form.description.data

            formAssets = {entry.data["ticker"]:entry.data["amount"]
                          for entry in form.assets.entries}
        
            newPortfolioAssets = set()
            # First update existing entries and delete
            for asset in portfolio.assets:
                ticker = asset.info.ticker
                if ticker in formAssets:
                    asset.amount = formAssets[ticker]
                    newPortfolioAssets.add(ticker)
                else:
                    db.session.delete(asset)

            for ticker, amount in formAssets.items():
                if ticker not in newPortfolioAssets:
                    portfolio.add_asset(ticker, amount)

            db.session.commit()
            
        return redirect(url_for('portfolio.view_portfolio', id=id))

    form.name.data = portfolio.name
    form.description.data = portfolio.description

    for i in range(len(form.assets)):
        form.assets.pop_entry()
        
    for asset in portfolio.assets:
        form.assets.append_entry({"ticker": asset.info.ticker,
                                  'amount': asset.amount})

    return render_template('edit-portfolio.html',
                           _title="Edit Portfolio",
                           portfolio = portfolio,
                           form=form)
    
