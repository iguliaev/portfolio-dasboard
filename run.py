# -*- encoding: utf-8 -*-
"""
MIT License
Copyright (c) 2019 - present AppSeed.us
"""

from flask_migrate import Migrate
from os import environ
from sys import exit
from decouple import config

from config import config_dict
from app import create_app, db, cli
from app.models import User, Portfolio, Asset, AssetInfo, AssetPrice

# WARNING: Don't run with debug turned on in production!
DEBUG = config('DEBUG', default=True)

# The configuration
get_config_mode = 'Debug' if DEBUG else 'Production'

try:
    
    # Load the configuration using the default values 
    app_config = config_dict[get_config_mode.capitalize()]

except KeyError:
    exit('Error: Invalid <config_mode>. Expected values [Debug, Production] ')

app = create_app( app_config ) 
Migrate(app, db)
cli.register(app)

@app.shell_context_processor
def make_shell_context():
    return {'db': db,
            'User': User,
            'Portfolio': Portfolio,
            'Asset': Asset,
            'AssetInfo': AssetInfo,
            'AssetPrice': AssetPrice}

if __name__ == "__main__":
    app.run()
